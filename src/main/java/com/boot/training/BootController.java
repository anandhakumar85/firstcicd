package com.boot.training;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BootController {

	@RequestMapping("/")
	public String Hello() {
		return "welcome";
	}
}
